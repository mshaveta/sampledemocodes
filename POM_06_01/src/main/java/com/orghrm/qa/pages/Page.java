package com.orghrm.qa.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

abstract public class Page {
	WebDriver driver;
	WebDriverWait wait;
	public Page(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
	}
	
	abstract public String getPageTitle();
	abstract public WebElement getElement(By locator);
	abstract public void waitForElement(By locator);
}
