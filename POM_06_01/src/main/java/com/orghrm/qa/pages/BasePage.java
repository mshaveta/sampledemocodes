package com.orghrm.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BasePage extends Page {

	public BasePage(WebDriver driver) {
		super(driver);
	}

	@Override
	public String getPageTitle() { 
		 String actualTitle = driver.getTitle();
		 return actualTitle;
	}

	@Override
	public WebElement getElement(By locator) {
		WebElement elm = null;
		waitForElement(locator);
		try {
			  elm = driver.findElement(locator);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return elm;
	}
	
	public boolean isElmPresent(By locator) {
		waitForElement(locator);
		boolean elmStatus = getElement(locator).isDisplayed();
		return elmStatus;
	}

	@Override
	public void waitForElement(By locator) {
		 
		wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		
	}

	 

}
