package com.orghrm.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage{

	By logout_btn = By.partialLinkText("Logout");
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement getLogout_btn() {
		WebElement lgBtnElm = getElement(logout_btn);
		return lgBtnElm;
	}
	
	public boolean chkLgBtnVisble() {
		boolean res = isElmPresent(logout_btn);//By.partialLinkText("Logout")
		return res;
	}
	public LoginPage doLogOut() { 
		getLogout_btn().click();
		return new LoginPage(driver);
	}

}
