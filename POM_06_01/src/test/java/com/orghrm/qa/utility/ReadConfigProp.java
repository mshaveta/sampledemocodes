package com.orghrm.qa.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadConfigProp {
	
	public static String readConfigProperty(String propName) {
		String propVal="";
		String configPath = System.getProperty("user.dir") + "\\config\\config.properties";
		try {
			FileInputStream fis = new FileInputStream(configPath);
			Properties prop = new Properties();
			try {
				prop.load(fis);
				propVal = prop.getProperty(propName);
				 
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return propVal;
	}
	
	 
}
