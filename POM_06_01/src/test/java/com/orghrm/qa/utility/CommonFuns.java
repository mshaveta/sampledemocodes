package com.orghrm.qa.utility;

public class CommonFuns {
	public static String[][] readData(String fileName, String sheetName) {
		String xlSheetPath = System.getProperty("user.dir") + "\\testData\\"+fileName;
		XlsReader xlr = null;
		try {
			xlr = new XlsReader(xlSheetPath);
		} catch (Exception e) {
			e.printStackTrace();
		}

		int rCount = xlr.getRowCount(sheetName);

		int cCount = xlr.getCellCount();

		String[][] dataArr = new String[rCount - 1][cCount];

		for (int i = 1; i < rCount; i++) {
			for (int j = 0; j < cCount; j++) { // 0,1
				dataArr[i - 1][j] = xlr.getCellData(i, j);
			}
		}
		return dataArr;
	}
}
