package com.orghrm.qa.utility;

import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 

public class XlsReader {

	private static Workbook wb;
	private static Sheet sh;
	private Row row;
	
	public XlsReader(String xlSheetPath) throws Exception {
		FileInputStream fis = new FileInputStream(xlSheetPath);
		wb = new XSSFWorkbook(fis);
	}

	public int getRowCount(String sheetName) {
		sh = wb.getSheet(sheetName);
		int lastRowNum = sh.getLastRowNum();//3
		int firstRowNum = sh.getFirstRowNum();//0
		int rCount = lastRowNum - firstRowNum + 1;
		return rCount;

	}
	
	public int getCellCount() {
		row = sh.getRow(0);
		int firstCellNum = row.getFirstCellNum();//0
		int lastCellNum = row.getLastCellNum();//2
		int cCount = lastCellNum - firstCellNum;
		return cCount;
	}

	public String getCellData(int r, int c) {
		String cellData = sh.getRow(r).getCell(c).getStringCellValue();
		return cellData;

	}
	
	public static String getCellData(String sheetName, int row, String colName) {
		Sheet sh = wb.getSheet(sheetName);
		Row rw = sh.getRow(0);
		int col_num = -1;
		int colCount = rw.getLastCellNum() - rw.getFirstCellNum();
		 
		for (int i = 0; i < colCount; i++) {
			//System.out.println("val-"+rw.getCell(i).getStringCellValue());
			boolean boolCellVal = rw.getCell(i).getStringCellValue().equalsIgnoreCase(colName);
			 //System.out.println(boolCellVal);
			if (boolCellVal) {
				col_num=i;
			}
		}
		
		//System.out.println("Col num:"+col_num);
		String cellVal = sh.getRow(row).getCell(col_num).getStringCellValue();
		return cellVal;
	}
	
	public static String getCellData(  int row, String colName) {
		 
		Row rw = sh.getRow(0);
		int col_num = -1;
		int colCount = rw.getLastCellNum() - rw.getFirstCellNum();
		 
		for (int i = 0; i < colCount; i++) {
			//System.out.println("val-"+rw.getCell(i).getStringCellValue());
			boolean boolCellVal = rw.getCell(i).getStringCellValue().equalsIgnoreCase(colName);
			 //System.out.println(boolCellVal);
			if (boolCellVal) {
				col_num=i;
			}
		}
		
		//System.out.println("Col num:"+col_num);
		String cellVal = sh.getRow(row).getCell(col_num).getStringCellValue();
		return cellVal;
	}

	
	

}
