package com.orghrm.qa.tests;

import static com.orghrm.qa.utility.ReadConfigProp.readConfigProperty;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.orghrm.qa.pages.LoginPage;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {
	WebDriver driver;
	LoginPage lp ;
	 
	@BeforeClass
	public void browserLaunch() {
		String browser = readConfigProperty("browser");
		System.out.println("Browser name - "+browser);
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		driver.get(readConfigProperty("appurl"));
		 
		lp= new LoginPage(driver);
	}
	
	
	@AfterClass
	public void closeBrowser() {
		driver.close();
	}
	

}
