package com.orghrm.qa.tests;

import static com.orghrm.qa.utility.CommonFuns.readData;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.orghrm.qa.utility.ReadConfigProp.*;

public class LoginTest extends BaseTest {

	@DataProvider
	public String[][]  getTestData() {
		String workBookName = readConfigProperty("testDatXlSh");
		String sheetName = readConfigProperty("sheet");
		String[][] dataArr = readData(workBookName,sheetName);
		
		
		return dataArr;
	}

	@Test(dataProvider = "getTestData")
	public void verifyLoginFunc(String username, String password) {
		lp.doLogin("Admin", username, password);
		
		
	}
}
