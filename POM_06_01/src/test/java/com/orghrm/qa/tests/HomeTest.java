package com.orghrm.qa.tests;

import static com.orghrm.qa.utility.ReadConfigProp.readConfigProperty;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.orghrm.qa.pages.HomePage;

public class HomeTest extends BaseTest {
	@Test
	public void verifyLogout() {
		//read username and password from config file
		String userRole = readConfigProperty("user_role");
		String username = readConfigProperty("username");
		String password = readConfigProperty("password");
		//Step1: Login
		HomePage hp = lp.doLogin(userRole,username,password); // loginPage class
		
		//Step2: Check Logout present
		
		boolean btnRes = hp.chkLgBtnVisble();//true
		Assert.assertTrue(btnRes, "Element is not Present. Plz chk");
		
		//Step3: Logout--- HomePage
		 lp = hp.doLogOut();
		  
	}
}
